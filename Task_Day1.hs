
--Volume of Cylinder
pi :: Float
pi = 3.14
doubleRadius r = r * r
Cyl_Vol :: Float -> Float -> Float
Cyl_Vol cylinderVolume = pi * doubleRadius r * h 

--alternate negative series
evenNegatives xs = [if (x mod 2) /= 0 then x else x = (-x) | x <- xs]


--find hypotenuse of a triangle 
double a = a * a
hypotenuse c = sqrt(double a + double b)  


--Take out second element in a list
xs = [1,2,3,4,5]
secondElement_list a = head (tail xs)


--Penultimate element in a list
xs = [1,2,3,4,5]
penultimateElement_list a = last(init xs)


--Celcius to Farenheit
celciusToFarenheit a = ((a * 9) / 5 ) + 32

--Temperature conversion
--Celcius to Kelvin
celciusToKelvin a = a + 273.15

--Kelvin To Farenheit
kelvinToFahrenheit b = 1.8 * (b - 273) + 32

--Kelvin To Celcius
kelvinToCelcius c = c - 273